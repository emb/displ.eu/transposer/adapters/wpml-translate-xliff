# import the baseclass for the adapter
# this class loads parses the config
# file at config_path
import basetranslateasyncadapter

# import the modules you need for your
# adapter implementation
import typing

from lxml import etree
from cerberus import Validator
import logging

# self-written modules
from adapter_utils import get_translation_targets

# config
INTERNAL_ENDPOINT_BASEDOMAIN = "internal_endpoint_basedomain"

# payload params
PROJECT_ID = "project_id"
JOB_ID = "job_id"
API_TOKEN = "api_token"
SOURCE_LANGUAGE = "source_language"
TARGET_LANGUAGE = "target_language"
FILENAME = "filename"


PAYLOAD_SCHEMA = {
    PROJECT_ID: {'type': 'string', 'required': True},
    JOB_ID: {'type': 'string', 'required': True},
    SOURCE_LANGUAGE: {'type': 'string', 'required': True},
    TARGET_LANGUAGE: {'type': 'string', 'required': True},
    FILENAME: {'type': 'string', 'required': True}
}

class Adapter(basetranslateasyncadapter.BaseTranslateAsyncAdapter):

    def __init__(self, connector: typing.Any, config_path: str | None = None) -> None:
        super(Adapter, self).__init__(connector, config_path)
        self.translation_target_topics = get_translation_targets(self.config.get("wpml_translate_xliff").get(INTERNAL_ENDPOINT_BASEDOMAIN).rstrip("/"))


    def run(self, payload, msg):
        print('This is the wpml translate xliff Adapter')
        print('wpml translate xliff ... run ... msg:', msg)

        result = {}
        try:
            validator = Validator(PAYLOAD_SCHEMA)
            validator.allow_unknown = True
            if validator.validate(payload) is False:
                # validator.errors could be for example {'input': ['required field'], 'format': ['unallowed value ee']}
                logging.error(f"validation error: {validator.errors}")
                result['success'] = False
                return result

            result[PROJECT_ID] = payload[PROJECT_ID]
            result[JOB_ID] = payload[JOB_ID]
            result[API_TOKEN] = payload[API_TOKEN]
            filename = payload[FILENAME]
            source_language =  payload[SOURCE_LANGUAGE]
            target_language_id =  payload[TARGET_LANGUAGE]

            translations_as_xliff = self.translate_xliff(filename, source_language, [target_language_id])

            for language_id  in translations_as_xliff:
                root_translations = translations_as_xliff[language_id]["payload"].getroot()
                target_filename = f"{target_language_id}-{filename}"
                etree.ElementTree(root_translations).write(target_filename, pretty_print=True)
                # print(f"Source: {source_text},\n Target: {target_text}")
                result["target_filename"] = target_filename
            
            # create result object
            result["success"] = self.translation_result_success
            
            if not self.translation_result_success:
                logging.error("Translation not successful")
                result['message'] = 'Translation not successful'
        except Exception: # probably best to remove this once it's stable enough and doesn't crash too often
            logging.exception("Exception thrown")
        
        return result


    def adapter_name(self):
        return 'wpml_translate_xliff'

